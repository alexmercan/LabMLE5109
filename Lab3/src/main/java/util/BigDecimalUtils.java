package util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public final class BigDecimalUtils {
    private BigDecimalUtils() {
    }

    public static BigDecimal sum(List<BigDecimal> numbers) {
        if (numbers == null) {
            return null;
        }

        return numbers
                .stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public static BigDecimal average(List<BigDecimal> numbers) {
        if (numbers == null) {
            return null;
        }

        if (numbers.isEmpty()) {
            return BigDecimal.ZERO;
        }

        return sum(numbers)
                .divide(BigDecimal.valueOf(numbers.size()), RoundingMode.HALF_EVEN);
    }

    public static List<BigDecimal> top10Percent(List<BigDecimal> numbers) {
        if (numbers == null) {
            return null;
        }

        long numberOfElements = (long) Math.floor(0.1 * numbers.size());

        return numbers
                .stream()
                .sorted(Comparator.reverseOrder())
                .limit(numberOfElements)
                .collect(Collectors.toList());
    }
}
