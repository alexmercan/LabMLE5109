package util;

import java.io.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public final class BigDecimalSerializationUtil {
    private BigDecimalSerializationUtil() {
    }

    public static <T> Stream<T> getObjectsFromFile(File f, Class<T> type) throws IOException {
        FileInputStream inputStream = new FileInputStream(f);
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
            return StreamSupport.stream(new Spliterators.AbstractSpliterator<T>(Long.MAX_VALUE, Spliterator.ORDERED) {
                @Override
                public boolean tryAdvance(Consumer<? super T> action) {
                    try {
                        @SuppressWarnings("unchecked")
                        var obj = (T) objectInputStream.readObject();
                        action.accept(obj);
                        return true;
                    } catch (IOException | ClassNotFoundException e) {
                        throw new RuntimeException(e);
                    }
                }

                @Override
                public Spliterator<T> trySplit() {
                    return null;
                }
            }, false).onClose(() -> {
                try {
                    objectInputStream.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (Throwable e) {
            try (FileInputStream toClose = inputStream) {
                throw e;
            }
        }
    }

    public static void writeObjectsToFile(File f, List<BigDecimal> objectsToWrite) {
        try (FileOutputStream outputStream = new FileOutputStream(f);
             ObjectOutputStream objectInputStream = new ObjectOutputStream(outputStream)) {
            for (var obj : objectsToWrite) {
                objectInputStream.writeObject(obj);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
