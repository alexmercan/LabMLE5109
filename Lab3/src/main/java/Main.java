import util.BigDecimalSerializationUtil;
import util.BigDecimalUtils;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Main {
    public static Long ELEM_COUNT = 500L;
    public static String FILENAME = "serialized.txt";


    public static void generateObjectsAndSerialize() {
        List<BigDecimal> numbers = new ArrayList<>();
        var rng = new Random();
        for (long i = 0; i < ELEM_COUNT; i++) {
            var number = Math.abs(rng.nextLong());
            numbers.add(BigDecimal.valueOf(Math.floorMod(number, 100000000L)));
        }

        BigDecimalSerializationUtil.writeObjectsToFile(new File(FILENAME), numbers);
    }

    public static void doOperations(List<BigDecimal> numbers) {
        System.out.printf("Average: %s\n", BigDecimalUtils.average(numbers));
        System.out.printf("Sum: %s\n", BigDecimalUtils.sum(numbers));
        var numberToStr = BigDecimalUtils.top10Percent(numbers)
                .stream()
                .map(BigDecimal::toString)
                .collect(Collectors.joining(", "));

        System.out.printf("Top 10%% : %s\n", numberToStr);
    }

    public static void main(String[] args) throws IOException {
        generateObjectsAndSerialize();
        var numbers = BigDecimalSerializationUtil.getObjectsFromFile(
                new File(FILENAME),
                BigDecimal.class).limit(ELEM_COUNT).collect(Collectors.toList()
        );

        doOperations(numbers);
    }
}