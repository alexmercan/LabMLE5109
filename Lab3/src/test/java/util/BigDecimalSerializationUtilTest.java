package util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

class BigDecimalSerializationUtilTest {
    private final String TEST_FILENAME = "test.txt";
    private final Long SIZE = 100L;
    private List<BigDecimal> inputNumbers;

    @BeforeEach
    void setup() {
        inputNumbers = new ArrayList<>();
        for (long i = 0; i < SIZE; i++) {
            inputNumbers.add(BigDecimal.valueOf(i));
        }
    }

    @Test
    void givenValidInput_whenWriteObjectsToFile_thenWriteToFile() throws IOException {
        BigDecimalSerializationUtil.writeObjectsToFile(new File(TEST_FILENAME), inputNumbers);

        var numbers =
                BigDecimalSerializationUtil.getObjectsFromFile(new File(TEST_FILENAME), BigDecimal.class)
                        .limit(SIZE).collect(Collectors.toList());
        assertThat(numbers).usingDefaultElementComparator().isEqualTo(inputNumbers);
    }
}