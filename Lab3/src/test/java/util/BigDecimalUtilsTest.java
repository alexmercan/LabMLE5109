package util;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class BigDecimalUtilsTest {
    private static final List<BigDecimal> input = List.of(
            BigDecimal.valueOf(99), BigDecimal.valueOf(0), BigDecimal.valueOf(421),
            BigDecimal.valueOf(1), BigDecimal.valueOf(21), BigDecimal.valueOf(99),
            BigDecimal.valueOf(22), BigDecimal.valueOf(31), BigDecimal.valueOf(2),
            BigDecimal.valueOf(6), BigDecimal.valueOf(67)
    );

    @Test
    void givenNullInput_whenSum_thenReturnNull() {
        assertThat(BigDecimalUtils.sum(null)).isNull();
    }

    @Test
    void givenValidInput_whenSum_thenReturnCorrectOutput() {
        assertThat(BigDecimalUtils.sum(input)).isEqualTo(BigDecimal.valueOf(769));
    }

    @Test
    void givenNullInput_whenAverage_thenReturnNull() {
        assertThat(BigDecimalUtils.average(null)).isNull();
    }

    @Test
    void givenEmpty_whenAverage_thenReturnZero() {
        assertThat(BigDecimalUtils.average(List.of())).isEqualTo(BigDecimal.ZERO);
    }

    @Test
    void givenValidInput_whenAverage_thenReturnCorrectOutput() {
        assertThat(BigDecimalUtils.average(input)).isEqualTo(BigDecimal.valueOf(70));
    }

    @Test
    void givenNullInput_whenTop10Percent_thenReturnNullOutput() {
        assertThat(BigDecimalUtils.top10Percent(null)).isNull();
    }

    @Test
    void givenValidInput_whenTop10Percent_thenReturnCorrectOutput() {
        assertThat(BigDecimalUtils.top10Percent(input)).isEqualTo(List.of(BigDecimal.valueOf(421)));
    }
}