package controller;

import lombok.AllArgsConstructor;
import service.OperationService;
import validator.ValidatorException;

import java.util.Scanner;

@AllArgsConstructor
public class CalculatorController {
    private static final String EXIT_STRING = "exit";
    private final OperationService operationService;
    private final Scanner scanner = new Scanner(System.in);

    public void run() {
        while (true) {
            System.out.print(">> ");
            var line = scanner.nextLine();
            if (line.equals(EXIT_STRING)) {
                System.out.println("Exiting calculator");
                break;
            }
            try {
                System.out.println(operationService.evaluate(line));
            } catch (ValidatorException validatorException) {
                System.out.println(validatorException.getMessage());
            }
        }
    }
}
