package validator;

public interface Validator<T> {
    /**
     * Validates the given entity
     *
     * @param entity the entity to validate
     */
    void validate(T entity);
}
