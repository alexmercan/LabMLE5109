package validator;

import java.util.regex.Pattern;

public class OperationValidatorImpl implements Validator<String> {
    private static final Pattern OPERATION_PATTERN = Pattern.compile("[+-]?[1-9]\\d*(.\\d+)? +[+\\-*/] +[+-]?[1-9]\\d*(.\\d+)?");

    @Override
    public void validate(String entity) {
        if (!OPERATION_PATTERN.matcher(entity).matches()) {
            throw new ValidatorException("Invalid operation");
        }
    }
}
