package service;

public interface OperationService {
    /**
     * Evaluates the given operation string
     *
     * @param operation the operation string to evaluate with the format ( number [+-/*] number )
     * @return the result of the given operation
     * @throws validator.ValidatorException if the operation is not valid
     */
    Double evaluate(String operation);
}
