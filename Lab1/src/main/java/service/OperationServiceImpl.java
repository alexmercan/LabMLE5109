package service;

import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import validator.Validator;

@AllArgsConstructor
public class OperationServiceImpl implements OperationService {
    private static final String PLUS = "+";
    private static final String MINUS = "-";
    private static final String MULTIPLY = "*";
    private final Validator<String> operationValidator;

    @Override
    public Double evaluate(String operation) {
        operationValidator.validate(operation);
        var operationParts = StringUtils.split(operation);
        // We know we will have 3 parts because we've already validated the operation
        var number1 = Double.parseDouble(operationParts[0]);
        var operationType = operationParts[1];
        var number2 = Double.parseDouble(operationParts[2]);

        switch (operationType) {
            case PLUS:
                return number1 + number2;
            case MINUS:
                return number1 - number2;
            case MULTIPLY:
                return number1 * number2;
            default:
                return number1 / number2;
        }
    }
}
