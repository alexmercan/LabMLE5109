import controller.CalculatorController;
import service.OperationServiceImpl;
import validator.OperationValidatorImpl;

public class Calculator {
    public static void main(String[] args) {
        var operationValidator = new OperationValidatorImpl();
        var operationService = new OperationServiceImpl(operationValidator);
        var calculatorController = new CalculatorController(operationService);
        calculatorController.run();
    }
}
