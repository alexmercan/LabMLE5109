package validator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class OperationValidatorImplTest {
    private Validator<String> underTest;

    @BeforeEach
    void setUp() {
        underTest = new OperationValidatorImpl();
    }

    @Test
    void givenValidOperations_whenValidate_thenDontThrow() {
        // given
        final var inputs = List.of(
                "3 + 4",
                "3.22 + 51.2",
                "55 * 21.2",
                "55.512 * 33.2",
                "22 / 2",
                "22.331 / 22.331"
        );

        // when
        inputs.forEach(input -> underTest.validate(input));

        // then
    }

    @Test
    void givenInvalidOperations_whenValidate_thenThrowException() {
        // given
        final var inputs = List.of(
                "",
                "     ",
                "3+34",
                "5 +",
                "5. * 32",
                "55",
                "67.",
                "5 / 23.",
                "/",
                "+",
                "-",
                "*"
        );

        // when
        var throwables = inputs
                .stream()
                .map(input -> catchThrowable(() -> underTest.validate(input)))
                .collect(Collectors.toList());

        // then
        throwables.forEach(throwable -> {
            assertThat(throwable).isInstanceOf(ValidatorException.class);
        });
    }
}