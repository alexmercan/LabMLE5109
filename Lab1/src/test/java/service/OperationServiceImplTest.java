package service;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import validator.Validator;
import validator.ValidatorException;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OperationServiceImplTest {

    private OperationService underTest;

    @Mock
    private Validator<String> validatorMock;

    @BeforeEach
    void setUp() {
        this.underTest = new OperationServiceImpl(validatorMock);
    }

    @Test
    void givenValidOperation_whenEvaluate_thenReturnCorrectResult() {
        // given
        final var INPUTS_EXPECTED = List.of(
                Pair.of("5 + 5", 10.0),
                Pair.of("5 * 5", 25.0),
                Pair.of("5 / 2", 2.5),
                Pair.of("5.5 * 3.0", 16.5),
                Pair.of("20.5 - 15.0", 5.5)
        );
        INPUTS_EXPECTED.forEach(inputExpected -> doNothing().when(validatorMock).validate(inputExpected.getLeft()));

        // when
        final var RESULTS_EXPECTED =
                INPUTS_EXPECTED
                        .stream()
                        .map(inputExpected -> Pair.of(underTest.evaluate(inputExpected.getLeft()), inputExpected.getRight()))
                        .collect(Collectors.toList());

        // then
        RESULTS_EXPECTED.forEach(
                result -> assertThat(result.getLeft()).isEqualTo(result.getRight())
        );
        INPUTS_EXPECTED.forEach(input_expected -> verify(validatorMock).validate(input_expected.getLeft()));
        verifyNoMoreInteractions(validatorMock);
    }

    @Test
    void givenInvalidOperation_whenEvaluate_thenThrowValidationException() {
        // given
        final var INPUT = "3. -3";
        doThrow(new ValidatorException("Invalid operation")).when(validatorMock).validate(INPUT);

        // when
        var result = catchThrowable(() -> underTest.evaluate(INPUT));

        // then
        assertThat(result)
                .isInstanceOf(ValidatorException.class)
                .hasMessage("Invalid operation");
        verify(validatorMock).validate(INPUT);
        verifyNoMoreInteractions(validatorMock);
    }
}