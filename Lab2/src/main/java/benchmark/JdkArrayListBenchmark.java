package benchmark;

import domain.Order;
import repository.InMemoryRepository;
import repository.JdkArrayListRepositoryImpl;

public class JdkArrayListBenchmark extends RepositoryBenchmark {
    @Override
    public InMemoryRepository<Order> getRepositoryImplementation() {
        return new JdkArrayListRepositoryImpl<>();
    }
}
