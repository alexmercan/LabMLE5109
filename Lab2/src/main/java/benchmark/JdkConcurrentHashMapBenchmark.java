package benchmark;

import domain.Order;
import repository.InMemoryRepository;
import repository.JdkConcurrentHashMapRepositoryImpl;

public class JdkConcurrentHashMapBenchmark extends RepositoryBenchmark {
    @Override
    public InMemoryRepository<Order> getRepositoryImplementation() {
        return new JdkConcurrentHashMapRepositoryImpl<>();
    }
}
