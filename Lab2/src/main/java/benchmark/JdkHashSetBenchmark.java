package benchmark;

import domain.Order;
import repository.InMemoryRepository;
import repository.JdkHashSetRepositoryImpl;

public class JdkHashSetBenchmark extends RepositoryBenchmark {
    @Override
    public InMemoryRepository<Order> getRepositoryImplementation() {
        return new JdkHashSetRepositoryImpl<>();
    }
}
