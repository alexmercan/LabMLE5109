package benchmark;

import domain.Order;
import repository.InMemoryRepository;
import repository.TroveTHashMapRepositoryImpl;

public class TroveTHashMapBenchmark extends RepositoryBenchmark {
    @Override
    public InMemoryRepository<Order> getRepositoryImplementation() {
        return new TroveTHashMapRepositoryImpl<>();
    }
}
