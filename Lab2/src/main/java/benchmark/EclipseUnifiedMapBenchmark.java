package benchmark;

import domain.Order;
import repository.EclipseUnifiedMapRepositoryImpl;
import repository.InMemoryRepository;

public class EclipseUnifiedMapBenchmark extends RepositoryBenchmark {
    @Override
    public InMemoryRepository<Order> getRepositoryImplementation() {
        return new EclipseUnifiedMapRepositoryImpl<>();
    }
}
