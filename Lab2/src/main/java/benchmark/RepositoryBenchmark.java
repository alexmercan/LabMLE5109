package benchmark;

import domain.Order;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import repository.InMemoryRepository;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 5, time = 1)
@Fork(1)
@State(Scope.Thread)
public abstract class RepositoryBenchmark {

    public InMemoryRepository<Order> underTest;

    @Param({"100", "1000", "10000", "100000"})
    public int repositoryNumberOfElems;
    public Order orderToRemove;
    public Order orderForContainsTest;
    public Order orderToAdd;

    @Setup(Level.Invocation)
    public void setup() {
        underTest = getRepositoryImplementation();
        for (int i = 0; i < repositoryNumberOfElems; i++) {
            underTest.add(new Order(i, i + 1, i + 2));
        }
        orderToAdd = new Order(repositoryNumberOfElems, 0, 0);
        // We'll just set this id to the 'middle' to try and avoid bias towards some implementations.
        orderForContainsTest = new Order(repositoryNumberOfElems / 2, 0, 0);
        orderToRemove = new Order(0, 0, 0);
    }

    @Benchmark
    public void add() {
        underTest.add(orderToAdd);
    }

    @Benchmark
    public void remove() {
        underTest.remove(orderToRemove);
    }

    @Benchmark
    public void contains(Blackhole blackhole) {
        blackhole.consume(underTest.contains(orderForContainsTest));
    }

    public abstract InMemoryRepository<Order> getRepositoryImplementation();
}
