package benchmark.primitive;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.Set;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 5, time = 1)
@Fork(1)
@State(Scope.Thread)
public abstract class PrimitiveSetBenchmark {

    public Set<Long> underTest;

    @Param({"100", "1000", "10000", "100000"})
    public int setSize;

    @Setup(Level.Invocation)
    public void setup() {
        underTest = getPrimitiveSetImplementation();
        for (long i = 0; i < setSize; i++) {
            underTest.add(i);
        }
    }

    @Benchmark
    public void add() {
        underTest.add(100L);
    }

    @Benchmark
    public void remove() {
        underTest.remove(0);
    }

    @Benchmark
    public void contains(Blackhole blackhole) {
        blackhole.consume(underTest.contains(setSize / 2L));
    }

    public abstract Set<Long> getPrimitiveSetImplementation();
}
