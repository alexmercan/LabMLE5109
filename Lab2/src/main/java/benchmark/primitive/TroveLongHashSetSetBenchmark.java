package benchmark.primitive;

import gnu.trove.decorator.TLongSetDecorator;
import gnu.trove.set.hash.TLongHashSet;

import java.util.Set;

public class TroveLongHashSetSetBenchmark extends PrimitiveSetBenchmark {
    @Override
    public Set<Long> getPrimitiveSetImplementation() {
        return new TLongSetDecorator(new TLongHashSet());
    }
}
