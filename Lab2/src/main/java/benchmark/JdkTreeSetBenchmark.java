package benchmark;

import domain.Order;
import repository.InMemoryRepository;
import repository.JdkTreeSetRepositoryImpl;

public class JdkTreeSetBenchmark extends RepositoryBenchmark {
    @Override
    public InMemoryRepository<Order> getRepositoryImplementation() {
        return new JdkTreeSetRepositoryImpl<>();
    }
}
