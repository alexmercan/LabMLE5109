package domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Order implements Identifiable<Integer>, Comparable<Order> {
    @EqualsAndHashCode.Include
    private Integer id;
    private int price;
    private int quantityAnInt;

    @Override
    public int compareTo(Order o) {
        return Integer.compare(this.id, o.getId());
    }
}
