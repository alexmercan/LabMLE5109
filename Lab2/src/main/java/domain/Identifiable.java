package domain;

public interface Identifiable<T> {
    T getId();
}
