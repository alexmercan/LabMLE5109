package repository;

import java.util.HashSet;

public class JdkHashSetRepositoryImpl<T> extends AbstractCollectionInMemoryRepository<T> {
    public JdkHashSetRepositoryImpl() {
        super(new HashSet<>());
    }
}
