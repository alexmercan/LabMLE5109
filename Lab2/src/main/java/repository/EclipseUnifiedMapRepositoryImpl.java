package repository;

import domain.Identifiable;
import org.eclipse.collections.impl.map.mutable.UnifiedMap;

public class EclipseUnifiedMapRepositoryImpl<Id, T extends Identifiable<Id>> extends AbstractMapInMemoryRepository<Id, T> {
    public EclipseUnifiedMapRepositoryImpl() {
        super(UnifiedMap.newMap());
    }
}
