package repository;


import java.util.TreeSet;

public class JdkTreeSetRepositoryImpl<T extends Comparable<T>> extends AbstractCollectionInMemoryRepository<T> {
    public JdkTreeSetRepositoryImpl() {
        super(new TreeSet<>());
    }
}
