package repository;

import domain.Identifiable;
import lombok.AllArgsConstructor;

import java.util.Map;

@AllArgsConstructor
public abstract class AbstractMapInMemoryRepository<Id, T extends Identifiable<Id>> implements InMemoryRepository<T> {
    private final Map<Id, T> elems;

    @Override
    public void add(T entity) {
        elems.putIfAbsent(entity.getId(), entity);
    }

    @Override
    public boolean contains(T entity) {
        return elems.containsKey(entity.getId());
    }

    @Override
    public void remove(T entity) {
        elems.remove(entity.getId());
    }
}
