package repository;

import domain.Identifiable;
import gnu.trove.map.hash.THashMap;

public class TroveTHashMapRepositoryImpl<Id, T extends Identifiable<Id>> extends AbstractMapInMemoryRepository<Id, T> {
    public TroveTHashMapRepositoryImpl() {
        super(new THashMap<>());
    }
}
