package repository;

import lombok.AllArgsConstructor;

import java.util.Collection;

@AllArgsConstructor
public abstract class AbstractCollectionInMemoryRepository<T> implements InMemoryRepository<T> {
    private final Collection<T> elems;

    @Override
    public void add(T entity) {
        if (!elems.contains(entity)) {
            elems.add(entity);
        }
    }

    @Override
    public boolean contains(T entity) {
        return elems.contains(entity);
    }

    @Override
    public void remove(T entity) {
        elems.remove(entity);
    }
}
