package repository;

import java.util.ArrayList;

public class JdkArrayListRepositoryImpl<T> extends AbstractCollectionInMemoryRepository<T> {
    public JdkArrayListRepositoryImpl() {
        super(new ArrayList<>());
    }
}
