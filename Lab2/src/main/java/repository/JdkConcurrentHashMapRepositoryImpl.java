package repository;

import domain.Identifiable;

import java.util.concurrent.ConcurrentHashMap;

public class JdkConcurrentHashMapRepositoryImpl<Id, T extends Identifiable<Id>> extends AbstractMapInMemoryRepository<Id, T> {
    public JdkConcurrentHashMapRepositoryImpl() {
        super(new ConcurrentHashMap<>());
    }
}
